<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable=['name'];

    public function subcategories(){
        return $this->hasMany('App\ProductSubcategory','category_id');
    }
}
