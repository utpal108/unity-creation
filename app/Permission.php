<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function module(){
        return $this->belongsTo('App\Module','module_id');
    }
}
