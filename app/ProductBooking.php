<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBooking extends Model
{
    protected $fillable=['user_id','product_id','product_variation_id','booking_time','quantity'];
}
