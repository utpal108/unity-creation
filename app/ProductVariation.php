<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
    protected $fillable=['product_id','color','sizes','regular_price','discount_price','min_quantity','wholesale_price','discount_wholesale_price','min_wholesale_quantity','sku','product_images'];

    protected $casts=['sizes'=>'array'];
}
