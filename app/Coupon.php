<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable=['coupon_code','description','discount_type','amount','expiry_date','limit_per_coupon','limit_per_user'];
}
