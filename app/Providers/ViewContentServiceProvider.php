<?php

namespace App\Providers;

use App\Blog;
use App\Menu;
use App\Product;
use App\Slider;
use App\Widget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ViewContentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //      For Slider
        view()->composer('partials.slider', function($view) {
            $sliders=Slider::all();
            $view->with(['sliders'=>$sliders]);
        });

        //        For Footer
        view()->composer('partials.footer', function($view) {
            $footer=Widget::where('name','footer')->first();
            $view->with(['footer'=>$footer]);
        });

        //	    For Menu
        view()->composer('partials.menu', function($view) {
            $items=Menu::display('admin');
            $view->with(['items'=>$items]);
        });

        //	    For Blog Posts
        view()->composer('templates.blog', function($view) {
            $blog_posts=Blog::paginate(15);
            $view->with(['blog_posts'=>$blog_posts]);
        });

        //	    For View Cart
        view()->composer('*', function($view) {
            $session_data=session('cart_data');
            $coupon_data=session('coupons_data');
            $product_charts=[];
            $discount_amount=0;
            if (!is_null($session_data)){
                $product_charts=json_decode($session_data,true);
            }
            if (!is_null($coupon_data)){
                $all_coupon_data=json_decode($coupon_data,true);
                foreach ($all_coupon_data as $coupon_data){
                    $discount_amount+=$coupon_data['discount_amount'];
                }
            }
            $view->with(['product_charts'=>$product_charts, 'discount_amount'=>$discount_amount]);
        });

        //	    For Trending Products
        view()->composer('partials.trending_products', function($view) {
            $products=Product::where('is_trend_product',1)->get();
            $view->with(['products'=>$products]);
        });

        //	    For News
        view()->composer('partials.news', function($view) {
            $blog_posts=Blog::take(3)->get()->toArray();
            $view->with(['blog_posts'=>$blog_posts]);
        });

        //	    For Common Settings
        view()->composer('*', function($view) {
            $regular_price_setting='regular_price';
            $discount_price_setting='discount_price';
            $min_quantity_setting='min_quantity';

            if ((Auth::check()) && (Auth::user()->user_type == 'wholesaler')){
                $regular_price_setting='wholesale_price';
                $discount_price_setting='discount_wholesale_price';
                $min_quantity_setting='min_wholesale_quantity';
            }

            $view->with(['regular_price_setting'=>$regular_price_setting, 'discount_price_setting'=>$discount_price_setting, 'min_quantity_setting'=>$min_quantity_setting]);
        });
    }
}
