<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    protected $fillable=['order_id','payment_method','currency','total_amount','status'];
}
