<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable=['title','subtitle','details','product_url','slider_image','slider_group_id'];

    public function slider_group(){
        return $this->belongsTo('App\SliderGroup','slider_group_id');
    }

    protected $casts=['product_url'=>'array'];
}
