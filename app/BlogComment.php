<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    protected $fillable=['blog_id','user_id','comment'];

    public function commented_by(){
        return $this->belongsTo('App\User','user_id');
    }

    public function replies(){
        return $this->hasMany('App\BlogCommentReplay','comment_id');
    }
}
