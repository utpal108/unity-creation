<?php
namespace App\Http\Controllers;
use App\Mail\OrderInvoice;
use App\OrderDetails;
use App\OrderPayment;
use App\Product;
use App\ProductOrder;
use App\ProductVariation;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Session;
use Illuminate\Routing\UrlGenerator;
use App\Http\Controllers;
use Validator;
use Illuminate\Validation\Rule;
session_start();

class PublicSslCommerzPaymentController extends Controller
{

    public function index(Request $request) 
    {
        $rules = [
            'billing_address.name'=>'required',
            'billing_address.email'=>'required|email',
            'billing_address.contact_no'=>'required',
            'billing_address.address'=>'required',
            'shipping_address.name'=>'required',
            'shipping_address.email'=>'required|email',
            'shipping_address.contact_no'=>'required',
            'shipping_address.address'=>'required',
        ];

        $messages = [
            'required' => ':attribute is required',
        ];

        $attributes = [
            'billing_address.name' => 'Billing name',
            'billing_address.email' => 'Billing email',
            'billing_address.contact_no' => 'Billing contact no',
            'billing_address.address' => 'Billing address',
            'shipping_address.name' => 'Shipping name',
            'shipping_address.email' => 'Shipping email',
            'shipping_address.contact_no' => 'Shipping contact no',
            'shipping_address.address' => 'Shipping address',
        ];

        $validation = Validator::make($request->all(), $rules, $messages, $attributes);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $allData=$request->all();
        $allData['order_by']=Auth::id();
        $allData['order_status']='pending';
        $product_order=ProductOrder::create($allData);
        $order_id='POR'.$product_order->id.date('dmys');
        $product_order->order_id=$order_id;
        $product_order->save();
        $total_amount=0;
        $session_data=session('cart_data');
        $coupon_data=session('coupons_data');
        $product_charts=[];
        $discount_amount=0;
        $coupon_codes='';
        $order_items=[];
        $order_total_price=0;

        if (!is_null($session_data)){
            $product_charts=json_decode($session_data,true);
        }

        if (!is_null($coupon_data)){
            $all_coupon_data=json_decode($coupon_data,true);
            foreach ($all_coupon_data as $coupon_data){
                $discount_amount+=$coupon_data['discount_amount'];
                $coupon_codes=$coupon_codes.$coupon_data['coupon_code'] .',';
            }
        }

        foreach ($product_charts as $index=>$product_chart){
            $regular_price=$product_chart['regular_price']*$product_chart['quantity'];
            $discount_price=$product_chart['unit_price']*$product_chart['quantity'];
            $total_price=$discount_price-$discount_amount;
            $total_amount+=$total_price;
            OrderDetails::create(['order_id'=>$product_order->id, 'product_id'=>$product_chart['id'],'product_quantity'=>$product_chart['quantity'],'product_size'=>$product_chart['product_size'],'product_color'=>$product_chart['color'],'coupon_code'=>$coupon_codes,'regular_price'=>$regular_price,'discount_price'=>$discount_price,'total_price'=>$total_price, 'status'=>'pending']);
            $product=Product::findOrFail($product_chart['id']);
            $order_items[$index]['product_code']=$product->product_code;
            $order_items[$index]['product_title']=$product->title;
            $order_items[$index]['price']=$discount_price;
            $order_items[$index]['quantity']=$product_chart['quantity'];
            $order_items[$index]['discount']=$discount_amount;
            $order_items[$index]['total_price']=$total_price;
            $order_total_price+=$total_price;
        }

        $order_payment=OrderPayment::create(['order_id'=>$product_order->id, 'currency'=>'BDT','total_amount'=>$total_amount, 'status'=>'pending']);

//        Send Invoice to user
        $order_info['order_id']=$order_id;
        $order_info['from']=setting('email');

        $pdf = PDF::loadView('product.invoice', compact('order_info','order_items','order_total_price'));
        $message = new OrderInvoice($order_info);
        $message->attachData($pdf->output(), "order_invoice.pdf");
        Mail::to(Auth::user()->email)->send($message);

            # Here you have to receive all the order data to initate the payment.
            # Lets your oder trnsaction informations are saving in a table called "orders"
            # In orders table order uniq identity is "order_id","order_status" field contain status of the transaction, "grand_total" is the order amount to be paid and "currency" is for storing Site Currency which will be checked with paid currency.

            $post_data = array();
            $post_data['total_amount'] = $total_amount; # You cant not pay less than 10
            $post_data['currency'] = "BDT";
            $post_data['tran_id'] = $product_order->order_id; // tran_id must be unique

            #Start to save these value  in session to pick in success page.
            $_SESSION['payment_values']['tran_id']=$post_data['tran_id'];
            #End to save these value  in session to pick in success page.


            $server_name=$request->root()."/";
            $post_data['success_url'] = $server_name . "success";
            $post_data['fail_url'] = $server_name . "fail";
            $post_data['cancel_url'] = $server_name . "cancel";

            # CUSTOMER INFORMATION
            $post_data['cus_name'] = Auth::user()->name;
            $post_data['cus_email'] = Auth::user()->email;
            $post_data['cus_add1'] = 'Customer Address';
            $post_data['cus_add2'] = "";
            $post_data['cus_city'] = "";
            $post_data['cus_state'] = "";
            $post_data['cus_postcode'] = "";
            $post_data['cus_country'] = "Bangladesh";
            $post_data['cus_phone'] = '8801XXXXXXXXX';
            $post_data['cus_fax'] = "";

            # SHIPMENT INFORMATION
            $post_data['ship_name'] = 'ship_name';
            $post_data['ship_add1 '] = 'Ship_add1';
            $post_data['ship_add2'] = "";
            $post_data['ship_city'] = "";
            $post_data['ship_state'] = "";
            $post_data['ship_postcode'] = "";
            $post_data['ship_country'] = "Bangladesh";

            # OPTIONAL PARAMETERS
            $post_data['value_a'] = "ref001";
            $post_data['value_b'] = "ref002";
            $post_data['value_c'] = "ref003";
            $post_data['value_d'] = "ref004";


            
            #Before  going to initiate the payment order status need to update as Pending.
//            $update_product = DB::table('orders')
//                                    ->where('order_id', $post_data['tran_id'])
//                                    ->update(['order_status' => 'Pending','currency' => $post_data['currency']]);

            $sslc = new SSLCommerz();
            # initiate(Transaction Data , false: Redirect to SSLCOMMERZ gateway/ true: Show all the Payement gateway here )
            $payment_options = $sslc->initiate($post_data, false);

            if (!is_array($payment_options)) {
                print_r($payment_options);
                $payment_options = array();
            }

    }

    public function success(Request $request) 
    {

        $sslc = new SSLCommerz();
        #Start to received these value from session. which was saved in index function.
        $tran_id = $_SESSION['payment_values']['tran_id'];
        #End to received these value from session. which was saved in index function.

        #Check order status in order tabel against the transaction id or order id.
        $order_detials=ProductOrder::where('order_id',$tran_id)->first();
        $order_payment=OrderPayment::where('order_id',$order_detials->id)->first();

        if($order_detials->order_status=='pending')
        {
            $validation = $sslc->orderValidate($tran_id, $order_payment->total_amount, $order_payment->currency, $request->all());
            if($validation == TRUE) 
            {
                /*
                That means IPN did not work or IPN URL was not set in your merchant panel. Here you need to update order status
                in order table as Processing or Complete.
                Here you can also sent sms or email for successfull transaction to customer
                */
                $order_detials->order_status='processing';
                $order_detials->save();
                $order_payment->status='paid';
                $order_payment->save();
                OrderDetails::where('order_id',$order_detials->id)->update(['status' => 'processing']);
                $order_details_data=OrderDetails::where('order_id',$order_detials->id)->get();
                foreach ($order_details_data as $order_data){
                    $product_variations=ProductVariation::where([['product_id',$order_data->product_id],['color',$order_data->product_color]])->firstOrFail();
                    $product_variations->sku=$product_variations->sku-$order_data->product_quantity;
                    $product_variations->save();
                }
                session(['cart_data' => null]);
                session(['coupons_data' => null]);
                session(['used_coupons' => null]);
                session()->flash('success_message','Order placed successfully');
                return redirect('/');
            }
            else
            {
                /*
                That means IPN did not work or IPN URL was not set in your merchant panel and Transation validation failed.
                Here you need to update order status as Failed in order table.
                */
                $order_detials->order_status='failed';
                $order_detials->save();
                $order_payment->status='failed';
                $order_payment->save();
                OrderDetails::where('order_id',$order_detials->id)->update(['status' => 'failed']);
                session()->flash('warning_message','Please complete payment to process order successfully');
                return redirect('/');
            }    
        }
        else if($order_detials->order_status=='processing' || $order_detials->order_status=='complete')
        {
            /*
             That means through IPN Order status already updated. Now you can just show the customer that transaction is completed. No need to udate database.
             */
            session(['cart_data' => null]);
            session(['coupons_data' => null]);
            session(['used_coupons' => null]);
            session()->flash('success_message','Order placed successfully');
            return redirect('/');
        }
        else
        {
             #That means something wrong happened. You can redirect customer to your product page.
            session()->flash('warning_message','Please complete payment to process order successfully');
            return redirect('/');
        }    
         


    }
    public function fail(Request $request) 
    {
         $tran_id = $_SESSION['payment_values']['tran_id'];
         $order_detials=ProductOrder::where('order_id',$tran_id)->first();
         $order_payment=OrderPayment::where('order_id',$order_detials->id)->first();

        if($order_detials->order_status=='Pending')
        {
            $order_detials->order_status='failed';
            $order_detials->save();
            $order_payment->status='failed';
            $order_payment->save();
            OrderDetails::where('order_id',$order_detials->id)->update(['status' => 'failed']);

            session()->flash('warning_message','Please complete payment to process order successfully');
            return redirect('/');
        }
         else if($order_detials->order_status=='processing' || $order_detials->order_status=='complete')
        {
            session(['cart_data' => null]);
            session(['coupons_data' => null]);
            session(['used_coupons' => null]);
            session()->flash('success_message','Order placed successfully');
            return redirect('/');
        }  
        else
        {
            session()->flash('warning_message','Please complete payment to process order successfully');
            return redirect('/');
        }        
                            
    }

     public function cancel(Request $request) 
    {
        $tran_id = $_SESSION['payment_values']['tran_id'];
        $order_detials=ProductOrder::where('order_id',$tran_id)->first();
        $order_payment=OrderPayment::where('order_id',$order_detials->id)->first();

        if($order_detials->order_status=='Pending')
        {
            $order_detials->order_status='canceled';
            $order_detials->save();
            $order_payment->status='canceled';
            $order_payment->save();
            OrderDetails::where('order_id',$order_detials->id)->update(['status' => 'canceled']);

            session()->flash('warning_message','Please complete payment to process order successfully');
            return redirect('/');
        }
         else if($order_detials->order_status=='processing' || $order_detials->order_status=='complete')
        {
            session(['cart_data' => null]);
            session(['coupons_data' => null]);
            session(['used_coupons' => null]);
            session()->flash('success_message','Order placed successfully');
            return redirect('/');
        }  
        else
        {
            session()->flash('warning_message','Please complete payment to process order successfully');
            return redirect('/');
        }                 

        
    }
     public function ipn(Request $request) 
    {
        #Received all the payement information from the gateway  
      if($request->input('tran_id')) #Check transation id is posted or not.
      {

          $tran_id = $request->input('tran_id');

        #Check order status in order tabel against the transaction id or order id.
          $order_details=ProductOrder::where('order_id',$tran_id)->first();
          $order_payment=OrderPayment::where('order_id',$order_details->id)->first();

                if($order_details->order_status =='Pending')
                {
                    $sslc = new SSLCommerz();
                    $validation = $sslc->orderValidate($tran_id, $order_payment->total_amount, $order_payment->currency, $request->all());
                    if($validation == TRUE) 
                    {
                        /*
                        That means IPN worked. Here you need to update order status
                        in order table as Processing or Complete.
                        Here you can also sent sms or email for successfull transaction to customer
                        */

                        $order_details->order_status='processing';
                        $order_details->save();
                        $order_payment->status='processing';
                        $order_payment->save();
                        OrderDetails::where('order_id',$order_details->id)->update(['status' => 'processing']);
                        session(['cart_data' => null]);
                        session(['coupons_data' => null]);
                        session(['used_coupons' => null]);

                        session()->flash('success_message','Order placed successfully');
                        return redirect('/');
                    }
                    else
                    {
                        /*
                        That means IPN worked, but Transation validation failed.
                        Here you need to update order status as Failed in order table.
                        */

                        $order_details->order_status='failed';
                        $order_details->save();
                        $order_payment->status='failed';
                        $order_payment->save();
                        OrderDetails::where('order_id',$order_details->id)->update(['status' => 'failed']);

                        session()->flash('warning_message','Please complete payment to process order successfully');
                        return redirect('/');
                    } 
                     
                }
                else if($order_details->order_status == 'processing' || $order_details->order_status =='complete')
                {
                    
                  #That means Order status already updated. No need to udate database.

                    session(['cart_data' => null]);
                    session(['coupons_data' => null]);
                    session(['used_coupons' => null]);
                    session()->flash('success_message','Order placed successfully');
                    return redirect('/');
                }
                else
                {
                   #That means something wrong happened. You can redirect customer to your product page.

                    session()->flash('warning_message','Please complete payment to process order successfully');
                    return redirect('/');
                }  
        }
        else
        {
            session()->flash('warning_message','Please complete payment to process order successfully');
            return redirect('/');
        }      
    }

}
