<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use App\ProductSubcategory;
use App\ProductVariation;
use App\ProductWiseCategory;
use App\ProductWiseSubcategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(Request $request){
        $search_option=$request->search_option;
        if ($search_option == 'high_to_low'){
            $products=Product::orderBy('average_price','desc')->paginate(15);
        }
        else{
            $products=Product::orderBy('average_price','asc')->paginate(15);
        }

        return view('templates.products',compact('products','search_option'));
    }
    public function product_details($id){
        $product=Product::findOrFail($id);
        $product_categories=ProductWiseCategory::where('product_id',$product->id)->get()->pluck('category_id')->all();
        $related_products_id=ProductWiseCategory::whereIn('category_id',$product_categories)->get()->pluck('product_id')->all();
        $related_products=Product::whereIn('id',$related_products_id)->where('id','<>',$id)->take(4)->get();
        return view('templates.product_details',compact('product','related_products'));
    }

    public function getSubcategories($id){
        $subcategories=ProductSubcategory::where('category_id',$id)->get();
        return view('ajax.getSubcategories',compact('subcategories'));
    }

    public function categoryWiseProducts($category){
        $category=str_replace('-',' ',$category);
        $product_category=ProductCategory::where('name',$category)->first();
        $category_wise_products=ProductWiseCategory::where('category_id',$product_category->id)->get()->pluck('product_id')->all();
        $products=Product::whereIn('id',$category_wise_products)->paginate(15);
        return view('templates.category_wise_products', compact('products','category'));
    }

    public function subcategoryWiseProducts($subcategory){
        $subcategory=str_replace('-',' ',$subcategory);
        $product_subcategory=ProductSubcategory::where('name',$subcategory)->firstOrFail();
        $subcategory_wise_products=ProductWiseSubcategories::where('subcategory_id',$product_subcategory->id)->get()->pluck('product_id')->all();
        $products=Product::whereIn('id',$subcategory_wise_products)->paginate(15);
        return view('templates.subcategory_wise_products', compact('products','category','subcategory'));
    }

    public function getProductVariations(Request $request){
        $product_variations=ProductVariation::where([['product_id',$request->product_id],['color',$request->product_color]])->first();
        $variation_id=$product_variations->id;
        $product_images=$product_variations->product_images;
        $product_sizes=$product_variations->sizes;
        $product_price=$product_variations->discount_price;
        $product_min_quantity=$product_variations->min_quantity;
        $product_max_quantity=$product_variations->sku;
        if ((Auth::check()) && (Auth::user()->user_type == 'wholesaler')){
            $product_price=$product_variations->discount_wholesale_price;
            $product_min_quantity=$product_variations->min_wholesale_quantity;
        }

        $variations=view('product.variations',compact('product_images','variation_id'))->render();
        $sizes=view('product.variation_sizes',compact('product_sizes'))->render();

        $quantity=view('product.variation_quantity',compact('product_min_quantity','product_max_quantity'))->render();
        return response()->json(['variations'=>$variations,'price'=>$product_price, 'sizes'=>$sizes, 'quantity'=>$quantity,'variation_id'=>$product_variations->id, 'sku'=>$product_max_quantity, 'min_quantity'=>$product_min_quantity]);
    }
}
