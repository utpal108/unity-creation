<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::where([['user_type','user'],['status','active']])->get();
        return view('admin.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=Role::all();
        return view('admin.user.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'email|required|unique:users',
            'password' => 'required',
            'country_code' => 'required',
            'phone_no' => 'required',
            'profession' => 'required',
            'address' => 'required',
        ]);

        $allData=$request->all();
        $allData['password']=bcrypt($request->password);

        if ($request->hasFile('profile_image')){
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $allData['profile_image']=$path;
        }
        $user=User::create($allData);
        $user->assignRole($request->roles);
        flash('User created successfully');
        return redirect()->action('Admin\UserController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles=Role::all();
        $user=User::find($id);
        return view('admin.user.edit',compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->country_code=$request->country_code;
        $user->phone_no=$request->phone_no;
        $user->user_type=$request->user_type;
        $user->profession=$request->profession;
        $user->address=$request->address;
        if (isset($request->password)){
            $user->password=bcrypt($request->password);
        }

        if ($request->hasFile('profile_image')){
            Storage::delete($user->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $user->profile_image=$path;
        }
        $user->save();
        $user->assignRole($request->roles);
        flash('User updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        flash('User deleted successfully');
        return redirect()->action('Admin\UserController@index');
    }

    public function pending_users(){
        $users=User::where('status','pending')->get();
        return view('admin.user.pending',compact('users'));
    }

    public function approve_user($id){
        $user=User::findOrFail($id);
        $user->status='active';
        $user->save();
        flash('User approved successfully');
        return redirect()->action('Admin\UserController@pending_users');
    }

    public function wholesalers(){
        $wholesalers=User::where([['status','active'],['user_type','wholesaler']])->get();
        return view('admin.user.wholesalers',compact('wholesalers'));
    }
}
