<?php

namespace App\Http\Controllers;

use App\BlogComment;
use App\BlogCommentReplay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogCommentController extends Controller
{
    public function store(Request $request){
        $allData=$request->all();
        $allData['user_id']=Auth::id();
        BlogComment::create($allData);
        $comments=BlogComment::where('blog_id',$request->blog_id)->get();
        return view('partials.blog_comment',compact('comments'));
    }

    public function replay(Request $request){
        $allData=$request->all();
        $allData['user_id']=Auth::id();
        BlogCommentReplay::create($allData);
        $comments=BlogComment::where('blog_id',$request->blog_id)->get();
        return view('partials.blog_comment',compact('comments'));
    }
}
