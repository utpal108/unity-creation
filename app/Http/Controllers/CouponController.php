<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function addUserCoupon(Request $request){
        $session_data=session('coupons_data');
        if (!is_null($session_data)){
            return response('You already used a coupon');
        }
        $coupon=Coupon::where('coupon_code',$request->coupon_code)->first();
        if (is_null($coupon)){
            return response('Invalid coupon code');
        }
        else{
            $remain_coupons=$coupon->limit_per_coupon - $coupon->used_coupon;
            if ($remain_coupons>=1){
                $session_used_coupons=session('used_coupons');
                if (!is_null($session_used_coupons)){
                    $already_used=json_decode($session_used_coupons,true);
                    if (in_array($request->coupon_code,$already_used)){
                        return response('Coupon already used');
                    }
                    else{
                        array_push($already_used,$request->coupon_code);
                        session(['coupons_data' => json_encode($already_used)]);
                    }
                }
                $coupon->used_coupon = $coupon->used_coupon +1;
                $coupon->save();
                $new_coupon=['coupon_code'=>$coupon->coupon_code, 'discount_amount'=>$coupon->amount];


//                if (!is_null($session_data)){
//                    $coupons_data=json_decode($session_data,true);
//                    array_push($coupons_data,$new_coupon);
//                    session(['coupons_data' => json_encode($coupons_data)]);
//                }
//                else{
//                    session(['coupons_data' => json_encode([$new_coupon])]);
//                }

                session(['coupons_data' => json_encode([$new_coupon])]);
                return response('Coupon code added successfully');
            }
            else{
                return response('Coupon code limit exceed');
            }
        }
    }

    public function usage(Request $request){
        $session_data=session('coupons_data');
        if (!is_null($session_data)){
            session()->flash('warning_message','You already used a coupon');
            return redirect()->back();
        }
        $coupon=Coupon::where('coupon_code',$request->coupon_code)->first();
        if (is_null($coupon)){
            session()->flash('warning_message','Invalid coupon code');
        }
        else{
            $remain_coupons=$coupon->limit_per_coupon - $coupon->used_coupon;
            if ($remain_coupons>=1){
                $session_used_coupons=session('used_coupons');
                if (!is_null($session_used_coupons)){
                    $already_used=json_decode($session_used_coupons,true);
                    if (in_array($request->coupon_code,$already_used)){
                        session()->flash('warning_message','Coupon already used');
                        return redirect()->back();
                    }
                    else{
                        array_push($already_used,$request->coupon_code);
                        session(['coupons_data' => json_encode($already_used)]);
                    }
                }
                $coupon->used_coupon = $coupon->used_coupon +1;
                $coupon->save();
                $new_coupon=['coupon_code'=>$coupon->coupon_code, 'discount_amount'=>$coupon->amount];


//                if (!is_null($session_data)){
//                    $coupons_data=json_decode($session_data,true);
//                    array_push($coupons_data,$new_coupon);
//                    session(['coupons_data' => json_encode($coupons_data)]);
//                }
//                else{
//                    session(['coupons_data' => json_encode([$new_coupon])]);
//                }

                session(['coupons_data' => json_encode([$new_coupon])]);

                session()->flash('success_message','Coupon code added successfully');
            }
            else{
                session()->flash('warning_message','Coupon code limit exceed');
            }
        }
        return redirect()->back();
    }
}
