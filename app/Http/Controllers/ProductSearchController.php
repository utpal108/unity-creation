<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductSearchController extends Controller
{
    public function index(Request $request){
        $search=$request->search;
        $products=Product::where('title','like','%'.$search.'%')->paginate(15);
        return view('templates.search_products',compact('products','search'));
    }
}
