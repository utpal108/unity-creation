<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogComment;
use Illuminate\Http\Request;

class BlogPostController extends Controller
{
    public function show($id){
        $blog_post=Blog::find($id);
        $comments=BlogComment::where('blog_id',$id)->get();
        return view('templates.blog_details',compact('blog_post','comments'));
    }
}
