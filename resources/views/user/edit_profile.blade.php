@extends('layout.app')

@section('page_title',' | Edit Profile')

@section('contents')
    <main class="page-main">
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit profile</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="profiles-setting py-0">
            <div class="container">
                <form class="profiles-setting-form" action="{{ action('UserController@update_profile') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="profile-image">
                        <img src="@if(!is_null($user->banner_image)){{ '/storage/' .$user->banner_image }} @else{{ '/images/Profiles_Setting.jpg' }} @endif" class="img-fluid banner-profile-img" id="profile-banner-image" alt="">
                        <div class="small-profile-img-div">
                            <img src="@if(!is_null($user->profile_image)){{ '/storage/' .$user->profile_image }} @else{{ '/images/profile-setting-small-img.png' }} @endif" id="profile-img-tag"/>
                            <input type="file" name="profile_image" id="profile-img" class="hidden">
                            <label for="profile-img" class="profile-img-label"><i class="fa fa-plus"></i><span>Add Image</span></label>
                        </div>
                        <div class="profile-large-img">
                            <input type="file" name="banner_image" id="profile-img2" class="hidden">
                            <label for="profile-img2" class="profile-img-label2 mb-0"><i class="fa fa-plus"></i><span>Add Image</span></label>
                        </div>
                    </div>
                    <div class="form-area-profile-settings">
                        <div class="row">
                            <div class="col-md-7 col-lg-7">
                                @include('flash::message')
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label for="fullNameProfilesSetting">Enter Your Full Name</label>
                                    <input type="text" name="name" value="{{ $user->name }}" class="form-control" id="fullNameProfilesSetting">
                                </div>
                                <div class="form-group">
                                    <label for="profileSettingEmail">Email Address</label>
                                    <input type="email" name="email" value="{{ $user->email }}" class="form-control" id="profileSettingEmail">
                                </div>
                                <div class="form-group">
                                    <label for="number">Phone number</label>
                                    <div class="row" style="margin: 0px .5px">
                                        <input type="text"  class="form-control col-md-3" name="country_code" value="{{ $user->country_code }}">
                                        <input type="number" value="{{ $user->phone_no }}" class="form-control col-md-9" name="phone_no" id="phoneNumber">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Job">Profession</label>
                                    <input type="text" name="profession" value="{{ $user->profession }}" class="form-control" id="Job">
                                </div>
                                <div class="form-group">
                                    <label for="address">address</label>
                                    <textarea class="form-control" id="address" name="address" rows="5">{{ $user->address }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="address">Change Password (Optional)</label>
                                    <input type="password" class="form-control" name="password" id="profileSettingChangePassword" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12" style='padding-left:0;padding-right:0;margin-bottom:120px'>
                                        <button type="submit" class="text-uppercase btn btn-primary profile-settings-btn-form mb-2">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>
@endsection

@section('script')
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
