@extends('layout.app')

@section('page_title','| News Details')

@section('contents')
    <main class="page-main">
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">News & Articles</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Details</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="blog-single p-0">
            <div class="container">
                <div class="col-12 pxs-0">
                    <h2 class="blog-heading">{{ $blog_post->title }}</h2>
                    <div class="post-info">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb ">
                                <li class="breadcrumb-item"><a href="#">{{ $blog_post->category['name'] }}</a></li>
                                <li class="breadcrumb-item"><a href="#">{{ $blog_post->post_created['name'] }}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{ $blog_post->created_at->diffForHumans() }}</li>
                            </ol>
                        </nav>
                    </div>
                    <img src="{{ '/storage/' .$blog_post->feature_image }}" class="img-fluid blog-img mb-3" alt="">
                    {!! $blog_post->details !!}
                    <div class="col-12 autthor-info">
                        <div class="row">
                            <div class="col-md-5 col-lg-6">
                                <div class="flex-box d-flex">
                                    <img src="{{ '/storage/' .$blog_post->post_created['profile_image']  }}" alt="">
                                    <div class="author-info">
                                        <a href="#" class="author-name">{{ $blog_post->post_created['name']  }}</a>
                                        <h5 class="small-txt">{{ $blog_post->post_created['profession']  }}</h5>
                                        <a href="mailto:{{ $blog_post->post_created['email'] }}?Subject=Contact%20message" class="author-contact">Email Me</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-6">
                                <h4 class="text-uppercase">Tags</h4>
                                <div class="tag-info d-flex">
								<span class=""><a href="#">Lorem,</a><span>
								<span class=""><a href="#">ipsum,</a><span>
								<span class=""><a href="#">sit amet,</a><span>
								<span class=""><a href="#">consectetur,</a><span>
								<span class=""><a href="#">adipiscing,</a><span>
								<span class=""><a href="#">elit,</a><span>
								<span class=""><a href="#">eiusmod,</a><span>
                                </div>
                                <div class="tag-info d-flex">
                                    <h4 class="text-uppercase">Share this post</h4>
                                    <div class="share-links">
                                        <ul class="list-inline d-flex social">
                                            <li><a href="{{ $blog_post->getShareUrl('facebook') }}" class="socialShare" target="_blank"><i class="fa fa-facebook icon-facebook"></i></a></li>
                                            <li><a href="{{ $blog_post->getShareUrl('twitter') }}" class="socialShare" target="_blank" ><i class="fa fa-twitter icon-twitter"></i></a></li>
                                            <li><a href="{{ $blog_post->getShareUrl('whatsapp') }}" class="socialShare" target="_blank" ><i class="fa fa-whatsapp icon-whatsapp"></i></a></li>
                                            <li><a href="{{ $blog_post->getShareUrl('pinterest') }}" class="socialShare" target="_blank"><i class="fa fa-pinterest icon-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="col-md-12 mt-5 post-links">--}}
{{--                    <div class="row">--}}
{{--                        <a href="#" class="img-links mr-auto">--}}
{{--                            <img src="/images/img-link1.jpg" alt="">--}}
{{--                            <span class="pre-post"><i class="fa fa-long-arrow-left"></i> Prev Post</span>--}}
{{--                        </a>--}}
{{--                        <a href="#" class="img-links ml-auto pull-right">--}}
{{--                            <img src="/images/img-link1.jpg" alt="">--}}
{{--                            <span class="pre-post">Next post <i class="fa fa-long-arrow-right"></i> </span>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="col-md-12 mt-5 comment-form">
{{--                    <h4 class="text-uppercase">(00) Comments</h4>--}}
{{--                    <h3 class="text-uppercase">Leave A Comments</h3>--}}
{{--                    <form id="" method="get" action="#">--}}
{{--                        <div class="col-12 p-0">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <input class="form-control" type="text" name="user-name" placeholder="FULL NAME">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <input class="form-control" type="text" name="user-email" placeholder="EMAIL ADDRESS">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <input class="form-control" type="text" name="user-web" placeholder="WEB SITE ADDRESS">--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <textarea rows="5" class="form-control user_comment" type="text" name="user-message" placeholder="ENTER YOUR COMMENTS"></textarea>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <button type="button" class="btn submit-btn text-uppercase" id="submit_comment">Submit Now</button>--}}
{{--                        </div>--}}
{{--                    </form>--}}


                    <div class="col-md-12 mt-5 comment-form full-box-msg-replay">
                        <span class="blog_comments">
                            @include('partials.blog_comment')
                        </span>

                        @if(!Auth::check())
                            <div class="form-group mt-3">
                                <a class="user-togg btn btn-success btn-sm" href="#" data-toggle="modal" data-target="#auth-modal">
                                    Please login or register first to leave comment
                                </a>
                            </div>
                        @endif

                        @if(Auth::check())
                            <h3 class="text-uppercase pt-5">Leave A Comments</h3>
                            <form method="post">
                                <div class="form-group">
                                    <textarea rows="5" class="form-control replay-comment-bottom user_comment" type="text" name="user-message" placeholder="Enter Your Comment"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="button" id="submit_comment" class="btn submit-btn text-uppercase" >Submit Now</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('script')
    <script>

        $('.ic_replay_message').on('click',function (e) {
            var comment=$(this).parent().prev().find('.comment_replay').val();
            var comment_id=$(this).attr('data-comment-id');
            var blog_id='{{ $blog_post->id }}';
            $.ajax({
                type:'POST',
                url:'/create-comment-replay',
                data:{blog_id: blog_id, comment_id: comment_id, replay: comment, _token: '{{ csrf_token() }}'},
                success:function (response) {
                    $(".blog_comments").html(response);
                    alert("Replayed successfully");
                }
            });
        });
        {{--  For Social Share --}}
        {{--  =====================================  --}}

        $('.socialShare').on('click',function (e) {
            e.preventDefault();
            var share_link=$(this).attr('href');
            window.open(share_link, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
        });

        $("#submit_comment").on('click',function () {
            var blog_id='{{ $blog_post->id }}';
            var comment= $('.user_comment').val();

            $.ajax({
                type:'POST',
                url:'/create-blog-comment',
                data:{blog_id: blog_id, comment: comment, _token: '{{ csrf_token() }}'},
                success:function (response) {
                    $(".blog_comments").html(response);
                    $('.user_comment').val('');
                    alert("Commented successfully");
                }
            });
        });


    </script>
@endsection
