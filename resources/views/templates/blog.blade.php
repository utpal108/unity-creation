@extends('layout.app')

@section('page_title','| ' .$page->page_title)

@section('contents')
    <main class="page-main">
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">News & Articles</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="blog py-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @foreach($blog_posts as $blog_post)
                            <div class="blog-post">
                                <h3 class="text-uppercase blog-post-heading">{{ $blog_post->title }}</h3>
                                <nav aria-label="breadcrumb" class="blog-breadcrumb">
                                    <ol class="breadcrumb latest-news-ol">
                                        <li class="breadcrumb-item active"><a href="#" class="text-uppercase latest-news">{{ $blog_post->category['name'] }}</a></li>
                                        <li class="breadcrumb-item"><span class="text-uppercase">{{ $blog_post->post_created['name'] }}</span></li>
                                        <li class="breadcrumb-item"><span class="text-uppercase">{{ $blog_post->created_at->diffForHumans() }}</span></li>
                                    </ol>
                                </nav>
                                <img src="{{ '/storage/' .$blog_post->feature_image }}" class="img-fluid" alt="">
                                <span class="mt-4 mb-0">
                                    {!! $blog_post->short_description !!}
                                </span>

                                <div class="col-md-12 p-0">
                                    <a href="{{ action('BlogPostController@show',$blog_post->id) }}" class="btn btn-cart btn-blog text-uppercase">read more</a>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-md-12 text-center justify-align-center bottom-pages pagination-blog">
                            <div class="row">
                                {{ $blog_posts->links('pagination.blog') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
