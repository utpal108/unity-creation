@extends('layout.app')

@section('page_title',' | Products')

@section('contents')
    <main class="page-main">
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item">{{ $category->name ?? '' }}</li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $subcategory ?? '' }}</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="product-items p-0">
            <div class="container">
                <div class="col-12 p-0 mt-3">
                    <div class="row">
                        @foreach($products as $product)
                            <div class="col-lg-3 col-md-4">
                                <div class="card product-card">
                                    <figure class="product-img" style="background:url({{ '/storage/'.$product->default_image }})"></figure>
                                    <div class="pro-info">
                                        <h5 class="text-uppercase pro-name"><a href="{{ action('ProductController@product_details',$product->id) }}">{{ $product->title }}</a></h5>
                                        <div class="col-12 p-0">
                                            <div class="row">
                                                <div class="col-7">
                                                    <p class="item-info mb-0">Style # {{ $product->product_code }}</p>
                                                    <span class="star-list">
                                                    <span class="fa fa-star {{ ($product->review['avg_rating'] <1) ? 'empty' : '' }}"></span>
                                                    <span class="fa fa-star {{ ($product->review['avg_rating'] <2) ? 'empty' : '' }}"></span>
                                                    <span class="fa fa-star {{ ($product->review['avg_rating'] <3) ? 'empty' : '' }}"></span>
                                                    <span class="fa fa-star {{ ($product->review['avg_rating'] <4) ? 'empty' : '' }}"></span>
                                                    <span class="fa fa-star {{ ($product->review['avg_rating'] <5) ? 'empty' : '' }}"></span>
                                                </span>
                                                </div>
                                                <div class="col-5">
                                                    <p class="item-info"><span class="d-block">{{ $product->product_variations->count() }} Colors</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 p-0 text-center">
                                            <h4 class="item-price text-left">${{ $product->average_price }}</h4>
                                        </div>
                                    </div>
                                </div><!-- PRODUCT-CARD-ENDS -->
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 text-center justify-align-center mt-4 mb-5 bottom-pages">
                    <div class="row">
                        {{ $products->links('pagination.products') }}
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

