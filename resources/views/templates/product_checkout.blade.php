@extends('layout.app')

@section('page_title',' | Product Checkout')

@section('contents')
    <main class="page-main">
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="cart-page p-0 mt-4">
            <div class="container">
                <div class="col-12 checkout-content mt-3 mb-5">
                    <div class="row">
                        <div class="col-md-6 order-md-2 mb-4"><!-- RIGHT-COLUM-STARTS -->
                            <div class="card total-card">
                                <div class="totals card-body">
                                    <h3 class="text-uppercase heading-cart">order Summery</h3>
                                    <div class="form-group">
                                        @php
                                            $total_amount=0;
                                        @endphp
                                        @foreach($product_charts as $index=>$product_chart)
                                            @php
                                                $product_details=product_details($product_chart['id']);
                                                $total_amount+=$product_chart['unit_price']*$product_chart['quantity'];
                                            @endphp
                                                <div class="row">
                                                    <div class="col-sm-2 hidden-xs"><img src="{{ '/storage/' .$product_details['default_image'] }}" alt="product image" class="img-fluid item-summary"></div>
                                                    <div class="col-sm-9 offset-md-1">
                                                        <h5 class="m-0 text-uppercase"><strong>{{ $product_details['title'] }}</strong></h5>
                                                        <p class="item-info m-0 green-txt">Style #: {{ $product_details['product_code'] }} / Size {{ $product_chart['product_size'] }}</p>
                                                        <h5 class="mt-2 mb-2 ">Quantity : {{ $product_chart['quantity'] }} <span class="pull-right">${{ $product_chart['unit_price']*$product_chart['quantity'] }}</span></h5>
                                                        <div class="row">
                                                            <div class="star-list col-6">
                                                                <span class="fa fa-star {{ ($product_chart['rating'] <5) ? 'empty' : '' }}"></span>
                                                                <span class="fa fa-star {{ ($product_chart['rating'] <4) ? 'empty' : '' }}"></span>
                                                                <span class="fa fa-star {{ ($product_chart['rating'] <3) ? 'empty' : '' }}"></span>
                                                                <span class="fa fa-star {{ ($product_chart['rating'] <2) ? 'empty' : '' }}"></span>
                                                                <span class="fa fa-star {{ ($product_chart['rating'] <1) ? 'empty' : '' }}"></span>
                                                            </div>
                                                            <p class="ml-auto col-6">(Based on {{ $product_chart['review'] }} Reviews)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                        @endforeach

                                    </div>
{{--                                    <div class="totals-item row">--}}
{{--                                        <label>Est. Delivery</label>--}}
{{--                                        <div class="totals-value" id="cart-subtotal">22 - 25 April, 2019</div>--}}
{{--                                    </div>--}}
                                    <div class="totals-item row">
                                        <label>Subtotal</label>
                                        <div class="totals-value" id="cart-tax">${{ $total_amount }}</div>
                                    </div>
                                    <div class="totals-item row">
                                        <label>Shipping Charge</label>
                                        <div class="totals-value" id="cart-shipping">$0</div>
                                    </div>
                                    <div class="totals-item totals-item-total row">
                                        <label>Tax</label>
                                        <div class="totals-value" id="cart-total">$0</div>
                                    </div>
                                    <div class="totals-item totals-item-total row">
                                        <label>Discount</label>
                                        <div class="totals-value" id="cart-total">${{ $discount_amount }}</div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="totals-item totals-item-total row">
                                        <label>Total</label>
                                        <div class="totals-value" id="cart-total">${{ $total_amount - $discount_amount }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 order-md-1 form-box">
                            @if(!Auth::check())
                                <div class="form-group mt-3">
                                    <a class="user-togg btn btn-success btn-sm" href="#" data-toggle="modal" data-target="#auth-modal">
                                        Please login or register first
                                    </a>
                                </div>
                            @endif
                            <h4 class="mb-3 h3 text-uppercase">Billing Address</h4>
                            <form class="needs-validation" action="{{ action('PublicSslCommerzPaymentController@index') }}" method="GET">
                                <div class="form-group no-mb">
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label for="firstName">Name <sup>*</sup></label>
                                            <input type="text" name="billing_address[name]" class="form-control" id="firstName" placeholder="Name" value="{{ Auth::user()->name ?? '' }}" required>
                                            <div class="invalid-feedback">
                                                Valid name is required.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group no-mb">
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                            <label>Email Address<sup>*</sup></label>
                                            <input type="email" name="billing_address[email]" class="form-control" placeholder="Email" value="{{ Auth::user()->email ?? '' }}" required>
                                            <div class="invalid-feedback">
                                                Valid email is required.
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label>Contact Number <sup>*</sup></label>
                                            <input type="text" name="billing_address[contact_no]" class="form-control" placeholder="Contact No" value="{{ Auth::user()->country_code ?? '' }}{{ Auth::user()->phone_no ?? '' }}" required>
                                            <div class="invalid-feedback">
                                                Valid contact no is required.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group no-mb">
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Address<sup>*</sup></label>
                                            <textarea name="billing_address[address]" class="form-control" required rows="5">{{ Auth::user()->address ?? '' }}</textarea>
                                            <div class="invalid-feedback">
                                                Valid address is required.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="mt-4 mb-3 h3 text-uppercase">Shipping Address</h4>
{{--                                <div class="form-group">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <ul class="list-inline check-list">--}}
{{--                                            <li><input id="male" type="checkbox" checked><label>Delivery address is the same as billing address</label></li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="form-group no-mb">
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Name <sup>*</sup></label>
                                            <input type="text" name="shipping_address[name]" class="form-control" placeholder="Name" value="" required>
                                            <div class="invalid-feedback">
                                                Valid name is required.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group no-mb">
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                            <label>Email Address<sup>*</sup></label>
                                            <input type="email" name="shipping_address[email]" class="form-control" placeholder="Email" value="" required>
                                            <div class="invalid-feedback">
                                                Valid email is required.
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label>Contact Number <sup>*</sup></label>
                                            <input type="text" name="shipping_address[contact_no]" class="form-control" placeholder="Contact No" value="" required>
                                            <div class="invalid-feedback">
                                                Valid contact no is required.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group no-mb">
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Address<sup>*</sup></label>
                                            <textarea name="shipping_address[address]" class="form-control" required rows="5"></textarea>
                                            <div class="invalid-feedback">
                                                Valid address is required.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(Auth::check())
                                    <div class="form-group mt-3">
                                        <button type="submit" class="btn btn-lg btn-block" >Continue to payment method</button>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </main>
@endsection
