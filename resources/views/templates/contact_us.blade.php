@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('style')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <style>
         /*CSS For error message*/
        .invalid-feedback {
            display: none;
            width: 100%;
            margin-top: .25rem;
            font-size: 80%;
            color: #e3342f;
        }
    </style>
@endsection

@section('contents')

    <main class="page-main">
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $page->page_title }}</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="contact py-0">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-4 col-lg-4">
                        <div class="single-div-contact px-3">
                            <h3 class="text-uppercase font-weight-bold heading-cart pb-2">{{ $page->contents['location_title'] ?? '' }}</h3>
                            <p>{{ $page->contents['location_details'] ?? '' }}</p>
                            <ul class="footer-links list-inline no-mb">
                                <li><i class="fa fa-phone"></i> {{ setting('phone_no') }}</li>
                                <li><i class="fa fa-map-marker"></i> {{ setting('address') }}</li>
                                <li><i class="fa fa-envelope"></i> {{ setting('email') }}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-4">
                        <div class="single-div-contact visit-hours px-3">
                            <h3 class="text-uppercase font-weight-bold heading-cart pb-2">{{ $page->contents['visit_hour_title'] ?? '' }}</h3>
                            {!! $page->contents['visit_hour_details'] ?? '' !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-4">
                        <div class="single-div-contact px-3">
                            <h3 class="text-uppercase font-weight-bold heading-cart pb-2">{{ $page->contents['connect_us_title'] ?? '' }}</h3>
                            <p>{{ $page->contents['connect_us_details'] ?? '' }}</p>
                            <div class="textwidget social contact-social">
                                <ul class="list-inline d-flex social">
                                    @foreach(json_decode(setting('social_icons'),true) as $social_icon)
                                        <li>
                                            <a href="{{ $social_icon['url'] ?? '' }}">
                                                <i class="{{ $social_icon['icon'] }}"></i>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="form-contact py-0">
            <div class="mt-5 comment-form mb-0">
                <div class="container">
                    <div class="row">
                        <div class="col-12 pdx-0">
                            <h3 class="text-uppercase font-weight-bold heading-cart pb-2 text-center contact-form-heading">have a question</h3>
                            <form action="{{ action('Admin\ContactMessageController@store') }}" method="post">
                                @csrf
                                <div class="col-12 p-0">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="FULL NAME" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="email" value="{{ old('email') }}" placeholder="EMAIL ADDRESS" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="title" value="{{ old('title') }}" placeholder="WEB SITE ADDRESS" required>
                                </div>
                                <div class="form-group">
                                    <textarea rows="5" class="form-control" type="text" name="message" placeholder="ENTER YOUR COMMENTS">{{ old('message') }}</textarea>
                                </div>
                                <div class="form-group pl-0 ml-0">
                                    <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"></div>
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn submit-btn text-uppercase">Submit Now</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="map p-0">
            <div id="map-canvas" class="m-0 p-0">
                <div id="map" style="width:100%; height:450px ;border:0px "></div>
                <!-- Replace the value of the key parameter with your own API key. -->
            </div>
        </section>
    </main>
@endsection

@section('script')
    <script>
        var map;
        function initMap() {
            var location = {lat: parseFloat({{ setting('current_lat') }}), lng: parseFloat({{ setting('current_long') }})};
            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 15, center: location});
            var marker = new google.maps.Marker({position: location, map: map});
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNrASOuKNnXr4a5rEqDC_8fo_isduYSeU&callback=initMap">
    </script>
@endsection
