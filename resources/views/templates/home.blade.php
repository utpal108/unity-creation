@extends('layout.app')

@section('style')

@endsection

@section('contents')

    @include('partials.slider')
    <!-- About -->
    <section class="popular-trend p-0 mt-5">
        <div class="container">
            <div class="col-12 p-0">
                <div class="card item-card item-card1" style="background:url({{ '/storage/' .$page->contents['best_fit_image'] ?? '' }})">
                    <div class="col-md-12 p-0">
                        <div class="row">
                            <div class="col-md-6 offset-md-6">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <h2 class="text-uppercase">{{ $page->contents['best_fit_title'] ?? '' }}</h2>
                                        <p>{{ $page->contents['best_fit_details'] ?? '' }}</p>
                                        <a href="{{ $page->contents['best_fit_url'] ?? '' }}" class="btn btn-lead text-uppercase" target="_blank">Shop Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 p-0 btm-panel">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="card item-card item-card2" style="background:url({{ '/storage/' .$page->contents['best_feel_image'] ?? '' }})">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-9 offset-md-3 pl-4 pxs-0">
                                        <div class="overlay-content">
                                            <div class="overlay-text">
                                                <h2 class="text-uppercase">{{ $page->contents['best_feel_title'] ?? '' }}</h2>
                                                <p>{{ $page->contents['best_feel_details'] ?? '' }}</p>
                                                <a href="{{ $page->contents['best_feel_url'] ?? '' }}" class="btn btn-lead text-uppercase" target="_blank">Shop Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card item-card item-card3" style="background:url({{ '/storage/' .$page->contents['best_quality_image'] ?? '' }})">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-12 text-center pxs-0">
                                        <div class="overlay-content">
                                            <div class="overlay-text">
                                                <h2 class="text-uppercase">{{ $page->contents['best_quality_title'] ?? '' }}</h2>
                                                <p>{{ $page->contents['best_quality_details'] ?? '' }}</p>
                                                <a href="{{ $page->contents['best_quality_url'] ?? '' }}" class="btn btn-lead text-uppercase" target="_blank">Shop Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('partials.trending_products')

    @include('partials.news')
    <!-- /About -->
@endsection

@section('script')
    @if(isset(json_decode(setting('special_offer'),true)['title']) && isset(json_decode(setting('special_offer'),true)['coupon_code']) && !is_null(json_decode(setting('special_offer'),true)['title'])&& !is_null(json_decode(setting('special_offer'),true)['coupon_code']))
        <script>
            $(window).on('load',function(){
                $('#AdsModal').modal('show');
            });
        </script>
    @endif
@endsection
