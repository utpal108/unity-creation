<section class="quality-products pt-5">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                @if(isset($blog_posts[0]) && !is_null($blog_posts[0]))
                    <div class="col-lg-6 col-md-12">
                        <div class="card fig-card" style="background:url({{ '/storage/medium/' .$blog_posts[0]['feature_image'] }})">
                            <div class="fig-span card-body">
                                <h4 class="text-uppercase">{{ $blog_posts[0]['title'] ?? '' }}</h4>
                                {!! \Illuminate\Support\Str::words($blog_posts[0]['short_description'], 30,' ....') !!}
                                <a href="{{ action('BlogPostController@show', $blog_posts[0]['id']) }}" class="link-aro">Learn More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-lg-6 right_bx">
                    @if(isset($blog_posts[1]) && !is_null($blog_posts[1]))
                        <div class="fig-card-sml card-top" style="background:url({{ '/storage/small/'.$blog_posts[1]['feature_image'] }})">
                            <div class="fig-span card-body">
                                <h4 class="text-uppercase">{{ $blog_posts[1]['title'] ?? '' }}</h4>
                                {!! \Illuminate\Support\Str::words($blog_posts[1]['short_description'], 30,' ....') !!}
                                <a href="{{ action('BlogPostController@show', $blog_posts[1]['id']) }}" class="link-aro">Learn More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    @endif
                    @if(isset($blog_posts[2]) && !is_null($blog_posts[2]))
                        <div class="fig-card-sml card-btm" style="background:url({{ '/storage/small/'.$blog_posts[2]['feature_image'] }})">
                            <div class="fig-span card-body">
                                <h4 class="text-uppercase">{{ $blog_posts[2]['title'] ?? '' }}</h4>
                                {!! \Illuminate\Support\Str::words($blog_posts[2]['short_description'], 30,' ....') !!}
                                <a href="{{ action('BlogPostController@show', $blog_posts[2]['id']) }}" class="link-aro">Learn More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</section>
