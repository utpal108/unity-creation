<footer class="footer">
    <div class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-md-4 eql-colmn">
                    <!-- Recent entries widget-->
                    <aside class="widget widget-recent-entries">
                        <div class="widget-title">
                            <h3 class="text-uppercase">{{ $footer->contents['section_one']['title'] ?? '' }}</h3>
                        </div>
                        <p>{{ $footer->contents['section_one']['details'] ?? '' }}</p>
                        <ul class="footer-links list-inline">
                            <li><i class="fa fa-phone"></i> {{ setting('phone_no') }}</li>
                            <li><i class="fa fa-map-marker"></i> {{ setting('address') }}</li>
                            <li><i class="fa fa-envelope"></i> {{ setting('email') }}</li>
                        </ul>
                        <div class="col-12 p-0">
                            <img src="/images/payment-m.png" class="img-fluid">
                        </div>
                    </aside>
                </div>
                <div class="col-md-3 eql-colmn">
                    <!-- Twitter widget-->
                    <aside class="widget widget-recent-entries">
                        <div class="widget-title">
                            <h3 class="text-uppercase">{{ $footer->contents['section_two']['title'] ?? '' }}</h3>
                        </div>
                        @if(isset($footer->contents['section_two']['quick_links']) && !is_null($footer->contents['section_two']['quick_links']))
                            <ul class="footer-links list-inline">
                                @foreach($footer->contents['section_two']['quick_links'] as $quick_link)
                                    <li><a href="{{ $quick_link['url'] ?? '' }}">{{ $quick_link['name'] ?? '' }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                    </aside>
                </div>
                <div class="col-md-5">
                    <!-- Text widget-->
                    <aside class="widget widget-text">
                        <div class="widget-title">
                            <h3 class="text-uppercase">{{ $footer->contents['section_three']['title'] ?? '' }}</h3>
                        </div>
                        <div class="newsletter-form">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscingi elitiumi. Vestibulum elementum sollicitudin purus nonni bibendum. </p>
                            <form action="{{ action('SubscriberController@store') }}" method="post" class="d-flex">
                                @csrf
                                <input type="email" class="form-control" name="email" placeholder="Email Address">
                                <button class="btn-sub" type="submit">Subscribe <i class="fa fa-paper-plane"></i></button>
                            </form>
                        </div>
                        <div class="textwidget social">
                            <ul class="list-inline d-flex social">
                                @foreach(json_decode(setting('social_icons'),true) as $social_icon)
                                    <li>
                                        <a href="{{ $social_icon['url'] ?? '' }}">
                                            <i class="{{ $social_icon['icon'] }}"></i>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="copyright">
                        <p>© {{ date('Y') }} unity creation. All Rights Reserved. Design & Developed By ITclan.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
