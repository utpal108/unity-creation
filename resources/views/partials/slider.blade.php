<section class="hero p-0">
    <div id="theme-sider" class="carousel slide theme-sider" data-ride="carousel">
        <!-- Indicators -->
        <!-- The slideshow -->
        <div class="carousel-inner">
            @foreach($sliders as $index=>$slider)
                <div class="carousel-item {{ ($index==0) ? 'active' : '' }}" style="background:url({{ '/storage/' .$slider->slider_image }})">
                    <div class="container">
                        <div class="crousal-caption">
                            <h4>{{ $slider->title ?? '' }}</h4>
                            <h1>{{ $slider->subtitle ?? '' }}</h1>
                            <h3>{{ $slider->details ?? '' }}</h3>
                            @if(!is_null($slider->product_url))
                                <a href="{{ $slider->product_url['url'] ?? '' }}" class="btn btn-lead">{{ $slider->product_url['name'] ?? '' }}</a>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#theme-sider" data-slide="prev">
            <i class="fa fa-angle-left"></i>
        </a>
        <a class="carousel-control-next" href="#theme-sider" data-slide="next">
            <i class="fa fa-angle-right"></i>
        </a>
    </div>
</section>
