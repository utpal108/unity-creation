    @if ($errors->any())
        <div class="alert alert-danger custom-alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <header id="header" class="navbar-expand-md">
    <div class="top-bar">
        <div class="container">
            <div class="col-12 p-0">
                <div class="row">
                    <div class="col-md-3 col-6">
                        <img src="/stotage/top-logo.png" alt="">
                    </div>
                    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-1 col-6">
                        <h5 class="tp-heading text-right"><span class="hide-xs">{{ setting('wholesale_title') }} </span><a href="{{ setting('wholesale_site_url') }}" class="btn btn-default visit-btn" target="_blank">{{ setting('wholesale_site_title') }}</a></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-light theme-navheader" id="sroll-class">
            <a class="navbar-brand d-lg-block d-md-block" href="/" title=""><img src="/storage/{{ setting('logo') }}" alt=""></a>
            <button class="navbar-toggler user-btn collapsed" type="button" data-toggle="collapse" data-target="#user-nav" aria-expanded="false">
                <span class="fa fa-user"></span>
            </button>
            <button class="navbar-toggler btn-toggle collapsed" type="button" data-toggle="collapse" data-target="#navBar" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="right-nav ml-auto collapse navbar-collapse user-nav" id="user-nav">
                <ul class="list-inline right-list ml-auto">
                    <li class="search-tog"><a href="#"><i class="fa fa-search"></i> <span class="search-txt">Search</span><span class="close-tog" style="display:none;">Close</span></a></li>
                    <li>
                        <div class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-map-marker"></i> Our Location</a>
                            <div class="dropdown-menu language-drop">
                                <a href="#nl"><img src="http://i65.tinypic.com/2d0kyno.png">Nederlands</a>
                                <a href="#en"><img src="http://i64.tinypic.com/fd60km.png">English</a>
                                <a href="#de"><img src="http://i63.tinypic.com/10zmzyb.png">German</a>
                                <a href="#fr"><img src="http://i65.tinypic.com/300b30k.png">Français</a>
                                <a href="#es"><img src="http://i68.tinypic.com/avo5ky.png">Español</a>
                                <a href="#it"><img src="http://i65.tinypic.com/23jl6bn.png">Italiano</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        @if(Auth::check())
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> {{ Auth::user()->name }}</a>
                                <div class="dropdown-menu language-drop">
                                    <a href="{{ action('UserController@profile') }}">Profile</a>
                                    <a href="{{ action('UserController@user_logout') }}">Logout</a>
                                </div>
                            </div>

                            <div class="dropdown-menu language-drop">
                                <a href="#nl"><img src="http://i65.tinypic.com/2d0kyno.png">Nederlands</a>
                            </div>
                        @else
                            <a class="user-togg" href="#" data-toggle="modal" data-target="#auth-modal">
                                <i class="fa fa-user-o"></i> Register Or Log In
                            </a>
                        @endif
                    </li>
                </ul>
                <div class="search-tp" style="display:none;">
                    <form class="d-flex" action="{{ action('ProductSearchController@index') }}" method="GET">
                        <input type="text" name="search" value="{{ $search ?? '' }}" placeholder="Search here.." required>
                        <button class="btn btn-search"><i class="fa fa-arrow-right"></i></button>
                    </form>
                </div>
            </div>
        </nav>
    </div>
    <div id="navBar" class="collapse navbar-collapse theme-nav">
        <div class="container d-flex nav-combo">
            @include('partials.menu')
            <ul class="navbar-nav u-header__navbar-nav cart-nav ml-auto">
                <li class="nav-item"><a href="#" data-toggle="modal" data-target="#cartModal"><i class="fa fa-shopping-cart"></i> cart  <span class="badge">@{{ total_items }}</span></a></li>
            </ul>
        </div>

    </div>
    <!-- #nav-menu-container -->
</header>
