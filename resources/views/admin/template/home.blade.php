<div class="widget-body">
    <fieldset>
        <legend>
            Form data for best fit product
        </legend>
        <div class="form-group">
            <label>Best fit product title</label>
            <input type="text" class="form-control" name="contents[best_fit_title]" value="{{ $page->contents['best_fit_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Best fit product details</label>
            <textarea rows="5" class="form-control" name="contents[best_fit_details]" required>{{ $page->contents['best_fit_details'] ?? '' }}</textarea>
        </div>
        <div class="form-group">
            <label>Best fit product URL</label>
            <input type="text" class="form-control" name="contents[best_fit_url]" value="{{ $page->contents['best_fit_url'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Best fit product image (557X533)</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['best_fit_image'])){{ '/storage/' .$page->contents['best_fit_image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="product image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="best_fit_image" @if(!isset($page->contents['best_fit_image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <legend>
            Form data for best feel product
        </legend>
        <div class="form-group">
            <label>Best feel product title</label>
            <input type="text" class="form-control" name="contents[best_feel_title]" value="{{ $page->contents['best_feel_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Best feel product details</label>
            <textarea rows="5" class="form-control" name="contents[best_feel_details]" required>{{ $page->contents['best_feel_details'] ?? '' }}</textarea>
        </div>
        <div class="form-group">
            <label>Best feel product URL</label>
            <input type="text" class="form-control" name="contents[best_feel_url]" value="{{ $page->contents['best_feel_url'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Best feel product Image (210X540)</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['best_feel_image'])){{ '/storage/' .$page->contents['best_feel_image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="product image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="best_feel_image" @if(!isset($page->contents['best_feel_image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <legend>
            Form data for best quality product
        </legend>
        <div class="form-group">
            <label>Best quality product title</label>
            <input type="text" class="form-control" name="contents[best_quality_title]" value="{{ $page->contents['best_quality_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Best quality product details</label>
            <textarea rows="5" class="form-control" name="contents[best_quality_details]" required>{{ $page->contents['best_quality_details'] ?? '' }}</textarea>
        </div>
        <div class="form-group">
            <label>Best quality product RL</label>
            <input type="text" class="form-control" name="contents[best_quality_url]" value="{{ $page->contents['best_quality_url'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Best quality product image (485X335)</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['best_quality_image'])){{ '/storage/' .$page->contents['best_quality_image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="product image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="best_quality_image" @if(!isset($page->contents['best_quality_image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

    </fieldset>

    @include('admin.template.partials.form_submit')
</div>



