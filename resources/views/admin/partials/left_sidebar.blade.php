
<div class="sa-aside-left">

    <a href="javascript:void(0)"  onclick="SAtoggleClass(this, 'body', 'sa-shortcuts-expanded')" class="sa-sidebar-shortcut-toggle">
        <img src="/ic_admin/img/avatars/sunny.png" alt="" class="online">
        <span>{{ Auth::user()->name }} <span class="fa fa-angle-down"></span></span>
    </a>
    <div class="sa-left-menu-outer">
        <ul class="metismenu sa-left-menu" id="menu1">
            {{--            For Dashboard           --}}
            <li class="{{ request()->is('admin') ? 'active' : '' }}">
                <a class="has-arrow"   href="{{ action('Admin\DashboardController@index') }}" title="Dashboard"><span class="fa fa-lg fa-fw fa-home"></span> <span class="menu-item-parent">Dashboard</span></a>
            </li>

            {{--            For Slider            --}}
            <li class="{{ request()->is('admin/slider*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Slider</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <!-- second-level -->
                    <li class="{{ request()->is('admin/slider-groups') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderGroupController@index') }}" title="Slider groups"> Slider groups </a>
                    </li><!-- second-level -->
                    <li class="{{ request()->is('admin/slider-groups/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderGroupController@create') }}" title="Create slider group"> Create slider group</a>
                    </li><!-- second-level -->
                    <li class="{{ request()->is('admin/sliders') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderController@index') }}" title="Sliders"> Sliders </a>
                    </li>
                    <li class="{{ request()->is('admin/sliders/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderController@create') }}" title="Create slider"> Create slider </a>
                    </li>
                </ul>

            </li>

            {{--            For Product            --}}

            <li class="{{ request()->is('admin/product*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Product"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Product</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/product-categories') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductCategoryController@index') }}" title="Product Categories"> Product Categories</a>
                    </li>
                    <li class="{{ request()->is('admin/product-categories/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductCategoryController@create') }}" title="Add Product Category"> Add Category</a>
                    </li>
                    <li class="{{ request()->is('admin/product-subcategories') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductSubcategoryController@index') }}" title="Product Subcategories"> Product Subcategories</a>
                    </li>
                    <li class="{{ request()->is('admin/product-subcategories/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductSubcategoryController@create') }}" title="Add Product Subcategory"> Add Subcategory</a>
                    </li>
                    <li class="{{ request()->is('admin/products') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductController@index') }}" title="Products"> Products </a>
                    </li>
                    <li class="{{ request()->is('admin/products/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductController@create') }}" title="Add Product"> Add Product </a>
                    </li>
                    <li class="{{ request()->is('admin/product-orders') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductOrderController@index') }}" title="Product Orders"> Product Orders </a>
                    </li>
                    <li class="{{ request()->is('admin/product-delivered') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductOrderController@delivered_products') }}" title="Product Delivered"> Product Delivered </a>
                    </li>
                </ul>

            </li>

            {{--            For Blog Posts  --}}

            <li class="{{ request()->is('admin/blog-posts*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Blog"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Blog</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/blog-posts') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\BlogController@index') }}" title="Blog Posts"> Blog Posts</a>
                    </li>
                    <li class="{{ request()->is('admin/blog-posts/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\BlogController@create') }}" title="Add Blog Post"> Add New Post</a>
                    </li>
                </ul>

            </li>

            {{--            For Pages           --}}

            <li class="{{ request()->is('admin/pages*') ? 'active' : '' }}"><!-- first-level -->
                <a class="has-arrow"   href="" title="Page"><span class="fa fa-lg fa-fw fa-file"></span> <span class="menu-item-parent"> Page</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/pages') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\PageController@index') }}" title="All pages"> All pages</a>
                    </li>
                    <li class="{{ request()->is('admin/pages/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\PageController@create') }}" title="Create new page"> Create new page</a>
                    </li>
                </ul>

            </li>

            {{--            For Menu Builder           --}}

            <li class="{{ request()->is('admin/menu*') ? 'active' : '' }}"><!-- first-level -->
                <a class="has-arrow"   href="" title="FAQ"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Menu</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/menu') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\MenuController@index') }}" title="menu builder"> Menu Builder </a>
                    </li>
                </ul>
            </li>

            {{--            For Site Settings          --}}

            <li class="{{ request()->is('admin/site-settings*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="FAQ"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Site Settings</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/site-settings') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SiteSettingController@index') }}" title="site settings"> Edit Settings </a>
                    </li>
                </ul>
            </li>

            {{--            For Widget        --}}

            <li class="{{ request()->is('admin/widgets*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Forum"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Widget</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/widgets') ? 'active' : '' }}">
                        <a href="{{ action('Admin\WidgetController@index') }}" title="widgets"> Widgets </a>
                    </li>
                </ul>
            </li>

            {{--            For Subscribers            --}}
            <li class="{{ request()->is('admin/subscribers*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Subscribers</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/subscribers') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SubscriberController@index') }}" title="All Subscribers"> Subscribers </a>
                    </li>
                    <li class="{{ request()->is('admin/subscribers/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SubscriberController@create') }}" title="Create subscriber"> Create Subscriber</a>
                    </li>
                </ul>
            </li>

            {{--            For Users           --}}
            <li class="{{ request()->is(['admin/users*','wholesalers*']) ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Users"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Users</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/users') ? 'active' : '' }}">
                        <a href="{{ action('Admin\UserController@index') }}" title="All Users"> All Users </a>
                    </li>
                    <li class="{{ request()->is('admin/users/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\UserController@create') }}" title="Create User"> Add User </a>
                    </li>
                    <li class="{{ request()->is('admin/users-pending') ? 'active' : '' }}">
                        <a href="{{ action('Admin\UserController@pending_users') }}" title="Pending Users"> Pending Users </a>
                    </li>
                    <li class="{{ request()->is('admin/wholesalers') ? 'active' : '' }}">
                        <a href="{{ action('Admin\UserController@wholesalers') }}" title="Wholesalers"> Wholesalers </a>
                    </li>
                </ul>
            </li>

            {{--            For Coupons           --}}
            <li class="{{ request()->is('admin/coupons*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Users"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Coupons</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/coupons') ? 'active' : '' }}">
                        <a href="{{ action('Admin\CouponController@index') }}" title="All Coupons"> All Coupons </a>
                    </li>
                    <li class="{{ request()->is('admin/coupons/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\CouponController@create') }}" title="Create Coupon"> Add New Coupon </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <a href="javascript:void(0)" class="minifyme" onclick="SAtoggleClass(this, 'body', 'minified')">
        <i class="fa fa-arrow-circle-left hit"></i>
    </a>
</div>
