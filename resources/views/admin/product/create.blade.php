@extends('admin.layout.app')

@section('page_title','Admin | Add new product')

@section('style')
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <style>
        /*body {*/
        /*    font-family:      "Segoe UI", Verdana, Helvetica, Arial, sans-serif;*/
        /*    font-size:        11px;*/
        /*    padding:        3em 8em 1em 4em;*/
        /*}*/

        #preview > * {
            box-shadow:       0 0 2em silver;
            padding:        2em;
        }

        .chapter {
            -webkit-columns:    460px;
            -moz-columns:    460px;
            columns:    460px;

            -webkit-column-gap:   4em;
            -moz-column-gap:   4em;
            column-gap:   4em;

            -webkit-column-rule:  thin solid silver;
            -moz-column-rule:  thin solid silver;
            column-rule:  thin solid silver;

            text-align:       justify;
        }

        /*h1,*/
        /*h2 {*/
        /*    background:       black;*/
        /*    color:          white;*/
        /*    padding:        .2em .4em;*/
        /*}*/
        /*h1 {*/
        /*    margin-top:       1em;*/
        /*}*/
        /*h2 {*/
        /*    background:       gray;*/
        /*}*/

        /*hr {*/
        /*    border-top:     double;*/
        /*    margin:       2em 25%;*/
        /*}*/

        #footer {
            margin-top:     4em;
            text-align:     center;
            color:        silver;
            border-top:     thin solid silver;
            padding-top:    1em;
        }

        .output {
            font-family:    monospace;
            border:       solid thin silver;
            padding:      .2em .4em;
            background-color: #cf3;
        }

        .clickable {
            cursor:       pointer;
        }

        pre {
            tab-size:     4;
            overflow-x:     auto;
            background-color: #eee;
            -webkit-column-break-inside: avoid;
        }
    </style>
    <link href="/ic_admin/css/jquery.colorpicker.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <style>
        /*remove button style*/
        .ic-thumb-item {
            position: relative;
            display: inline-block;
        }
        .ic-remove-btn {
            position: absolute;
            right: 10px;
            top: -3px;
            height: 15px;
            width: 15px;
            display: inline-block;
            background-color: #a94442;
            border-radius: 50%;
            cursor: pointer;
        }
        .ic-remove-btn:after {
            position: absolute;
            content: "";
            left: 1px;
            top: 6px;
            width: 80%;
            height: 2px;
            background-color: #fff;
        }
        /*remove button style /end */

    </style>
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\ProductController@index') }}">All Products</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Add new product</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <form id="sliderGroup" action="{{ action('Admin\ProductController@store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Add New Product </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>

                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Product Data
                                            </legend>
                                            <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" class="form-control" name="title" required/>
                                            </div>
                                            <div class="form-group">
                                                <label>Details</label>
                                                <textarea class="form-control editor" name="details" rows="8" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Average Price</label>
                                                <input type="number" class="form-control" name="average_price" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Is trend product</label>
                                                <select name="is_trend_product" class="form-control">
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            </div>

                                            <legend>Add Variation</legend>

                                            <div class="variationGroup">
                                                <div class="card variationElement" style="border: 1px solid grey; padding: 10px">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <button type="button" class="btn btn-danger float-right btn-sm" onclick="if(confirm('Are You Sure ?')){$(this).parent().parent().parent().remove()}"><i class="fa fa-remove"></i> </button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Select Color</label>
                                                        <div class="row">
                                                            <div class="col-md-3 mb-2 ml-0">
                                                                <div class="input-group">
                                                                    <input class="form-control colorpicker-full" type="text" name="product_variation[1][color]" autocomplete="off" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Available Sizes</label>
                                                        <div class="col-md-12">
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="S" class="checkbox style-0" name="product_variation[1][sizes][]">
                                                                <span>S</span>
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="M" class="checkbox style-0" name="product_variation[1][sizes][]">
                                                                <span>M</span>
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="L" class="checkbox style-0" name="product_variation[1][sizes][]">
                                                                <span>L</span>
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="XL" class="checkbox style-0" name="product_variation[1][sizes][]">
                                                                <span>XL</span>
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="2X" class="checkbox style-0" name="product_variation[1][sizes][]">
                                                                <span>2X</span>
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="3X" class="checkbox style-0" name="product_variation[1][sizes][]">
                                                                <span>3X</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Regular Price</label>
                                                                <input type="text" class="form-control" name="product_variation[1][regular_price]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Discount Price</label>
                                                                <input type="text" class="form-control" name="product_variation[1][discount_price]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Min Quantity</label>
                                                                <input type="text" class="form-control" name="product_variation[1][min_quantity]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Wholesale Price</label>
                                                                <input type="text" class="form-control" name="product_variation[1][wholesale_price]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Wholesale Discount Price</label>
                                                                <input type="text" class="form-control" name="product_variation[1][discount_wholesale_price]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Min Wholesale Quantity</label>
                                                                <input type="text" class="form-control" name="product_variation[1][min_wholesale_quantity]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Stock Keeping Unit</label>
                                                                <input type="number" class="form-control" name="product_variation[1][sku]" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="box-body">
                                                                    <div class="input-group">
                                                                      <span class="input-group-btn">
                                                                        <a data-input="thumbnail1" data-preview="holder1" class="btn btn-primary text-white lfm">
                                                                          <i class="fa fa-picture-o"></i> Choose
                                                                        </a>
                                                                      </span>
                                                                        <input id="thumbnail1" class="form-control" type="text" name="product_variation[1][product_images]" required>
                                                                    </div>
                                                                    <div id="holder1" style="margin-top:15px;max-height:100px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><br>
                                            </div>


                                            <div class="form-group">
                                                <button class="btn btn-primary" type="button" id="addVariation">Add Variation <i class="fa fa-plus-circle"></i></button>
                                            </div>

                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Create
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-3">
                            <!-- /well -->
                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-tags"></i> Default Image(537X647):</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-body text-center">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                    <img src="http://placehold.it/200x200" width="100%" alt="header image">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" name="default_image" required>
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /well -->

                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-tags"></i> Category:</h5>
                                <div class="row ml-1">
                                    <div class="col-lg-12">
                                        <dl>
                                            @foreach($product_categories as $product_category)
                                                <dd>
                                                    <div class="vcheck">
                                                        <label>
                                                            <input type="checkbox" class="checkbox style-0" name="product_category[]" value="{{ $product_category->id }}">
                                                            <span>{{ $product_category->name }}</span>
                                                        </label>
                                                    </div>
                                                </dd>
                                                @if(!is_null($product_category->subcategories))
                                                    @foreach($product_category->subcategories as $product_subcategory)
                                                        <dd>
                                                            <div class="vcheck ">
                                                                <label class="pl-4">
                                                                    <input type="checkbox" class="checkbox style-0" name="product_subcategory[]" value="{{ $product_subcategory->id }}">
                                                                    <span>{{ $product_subcategory->name }}</span>
                                                                </label>
                                                            </div>
                                                        </dd>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $('.lfm').filemanager('file');
        function removeImage(e){
            $(e).parent().remove();
            var targetImage=$(e).attr("data-imageSrc");
            var targetInput=$(e).attr("data-imageImput");
            var old_input=$('#'+targetInput).val();
            var new_imput=old_input.replace(targetImage,"").replace(',,',',');
            $('#'+targetInput).val(new_imput);
        };
    </script>
{{--    <script type="text/javascript" src="/ic_admin/js/bootstrap-colorpicker.min.js"></script>--}}

{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>--}}
{{--    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.js"></script>--}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/pagedown/1.0/Markdown.Converter.min.js"></script>
    <script src="/ic_admin/js/jquery.colorpicker.js"></script>
    <script class="prettyprint">
        $(function() {
            $('.colorpicker-full').colorpicker({
                parts:      'full',
                alpha:      true,
                showOn:     'both',
                buttonColorize: true,
                showNoneButton: true
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            if ($('.variationElement').length) {
                var variationElementCount = $('.variationElement').length+1;
            }
            else {
                var variationElementCount = 1;
            }

            $('#addVariation').click(function () {
                var variationData=`<div class="card variationElement" style="border: 1px solid grey; padding: 10px">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <button type="button" class="btn btn-danger float-right btn-sm" onclick="if(confirm('Are You Sure ?')){$(this).parent().parent().parent().remove()}"><i class="fa fa-remove"></i> </button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Select Color</label>
                                                        <div class="row">
                                                            <div class="col-md-3 mb-2 ml-0">
                                                                <div class="input-group">
                                                                    <input class="form-control colorpicker-full" type="text" name="product_variation[`+variationElementCount+`][color]" autocomplete="off" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Available Sizes</label>
                                                        <div class="col-md-12">
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="S" class="checkbox style-0" name="product_variation[`+variationElementCount+`][sizes][]">
                                                                <span>S</span>
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="M" class="checkbox style-0" name="product_variation[`+variationElementCount+`][sizes][]">
                                                                <span>M</span>
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="L" class="checkbox style-0" name="product_variation[`+variationElementCount+`][sizes][]">
                                                                <span>L</span>
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="XL" class="checkbox style-0" name="product_variation[`+variationElementCount+`][sizes][]">
                                                                <span>XL</span>
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="2X" class="checkbox style-0" name="product_variation[`+variationElementCount+`][sizes][]">
                                                                <span>2X</span>
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" value="3X" class="checkbox style-0" name="product_variation[`+variationElementCount+`][sizes][]">
                                                                <span>3X</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Regular Price</label>
                                                                <input type="text" class="form-control" name="product_variation[`+variationElementCount+`][regular_price]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Discount Price</label>
                                                                <input type="text" class="form-control" name="product_variation[`+variationElementCount+`][discount_price]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Min Quantity</label>
                                                                <input type="text" class="form-control" name="product_variation[`+variationElementCount+`][min_quantity]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Wholesale Price</label>
                                                                <input type="text" class="form-control" name="product_variation[`+variationElementCount+`][wholesale_price]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Wholesale Discount Price</label>
                                                                <input type="text" class="form-control" name="product_variation[`+variationElementCount+`][discount_wholesale_price]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Min Wholesale Quantity</label>
                                                                <input type="text" class="form-control" name="product_variation[`+variationElementCount+`][min_wholesale_quantity]" required />
                                                            </div>
                                                            <div class="col-lg-6 has-feedback">
                                                                <label>Stock Keeping Unit</label>
                                                                <input type="number" class="form-control" name="product_variation[`+variationElementCount+`][sku]" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="box-body">
                                                                    <div class="input-group">
                                                                      <span class="input-group-btn">
                                                                        <a data-input="thumbnail`+variationElementCount+`" data-preview="holder`+variationElementCount+`" class="btn btn-primary text-white lfm">
                                                                          <i class="fa fa-picture-o"></i> Choose
                                                                        </a>
                                                                      </span>
                                                                        <input id="thumbnail`+variationElementCount+`" class="form-control" type="text" name="product_variation[`+variationElementCount+`][product_images]" required>
                                                                    </div>
                                                                    <div id="holder`+variationElementCount+`" style="margin-top:15px;max-height:100px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><br>`;

                $('.variationGroup').append(variationData);
                variationElementCount++;
                $('.lfm').filemanager('file');
                $('.colorpicker-full').colorpicker({
                    parts:      'full',
                    alpha:      true,
                    showOn:     'both',
                    buttonColorize: true,
                    showNoneButton: true
                });
            });

            var editor_config = {
                path_absolute : "/",
                selector: "textarea.editor",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });
                }
            };

            tinymce.init(editor_config);

        })
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            /*
			 * CLOCKPICKER
			 */


            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    title : {
                        validators : {
                            notEmpty : {
                                message : 'Product title is required'
                            },
                        }
                    },
                    price : {
                        validators : {
                            notEmpty : {
                                message : 'Product price is required'
                            }
                        }
                    },
                    sku : {
                        validators : {
                            notEmpty : {
                                message : 'Product sku is required'
                            }
                        }
                    }
                    ,
                    details : {
                        validators : {
                            notEmpty : {
                                message : 'Product details is required'
                            }
                        }
                    },
                    product_image : {
                        validators : {
                            notEmpty : {
                                message : 'Product image is required'
                            }
                        }
                    }
                }
            });

            // end profile form


        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
