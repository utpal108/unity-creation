@extends('admin.layout.app')

@section('page_title','Admin | Product order details')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\ProductCategoryController@index') }}">product order</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Product order details</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">
                <!-- row -->
                <div class="row">

                    <!-- NEW WIDGET ROW START -->
                    <div class="col-md-12">

                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                            <!-- widget options:
                            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                            data-widget-colorbutton="false"
                            data-widget-editbutton="false"
                            data-widget-togglebutton="false"
                            data-widget-deletebutton="false"
                            data-widget-fullscreenbutton="false"
                            data-widget-custombutton="false"
                            data-widget-collapsed="true"
                            data-widget-sortable="false"

                            -->
                            <header>
                                <div class="widget-header">
                                    <h2>Product order details</h2>
                                </div>
                            </header>

                            <!-- widget div-->

                            <div>
                                <!-- widget content -->
                                <div class="widget-body">
                                    <fieldset>
                                        @foreach($order_items as $item)
                                            <div class="form-group row">
                                                <label class="col-md-2 control-label">Product Title : </label>
                                                <div class="col-md-10">
                                                    {{ $item->product['title'] }}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 control-label">Size : </label>
                                                <div class="col-md-10">
                                                    {{ $item->product_size }}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 control-label">Color : </label>
                                                <div class="col-md-10">
                                                    <div style="width: 20px; height: 20px; background-color: {{ '#'.$item->product_color }}"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 control-label">Quantity : </label>
                                                <div class="col-md-10">
                                                    {{ $item->product_quantity }}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 control-label">Regular Price : </label>
                                                <div class="col-md-10">
                                                    {{ $item->regular_price }}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 control-label">Discount Price : </label>
                                                <div class="col-md-10">
                                                    {{ $item->discount_price }}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 control-label">Coupon Codes : </label>
                                                <div class="col-md-10">
                                                    {{ $item->coupon_code }}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-2 control-label">Total Price : </label>
                                                <div class="col-md-10">
                                                    {{ $item->total_price }}
                                                </div>
                                            </div><hr>
                                        @endforeach

                                        <div class="form-group row">
                                            <label class="col-md-2 control-label">Order By: </label>
                                            <div class="col-md-10">
                                                {{ $product_order->ordered_by['name'] }}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 control-label">Billing Address : </label>
                                            <div class="col-md-10">
                                                <textarea class="form-control" placeholder="Billing Address" rows="4" disabled>Name : {{ $product_order->billing_address['name'] }} , Email : {{ $product_order->billing_address['email'] }}, Contact No : {{ $product_order->billing_address['contact_no'] }}, Address : {{ $product_order->billing_address['address'] }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 control-label">Shipping Address : </label>
                                            <div class="col-md-10">
                                                <textarea class="form-control" placeholder="Shipping Address" rows="4" disabled>Name : {{ $product_order->shipping_address['name'] }} , Email : {{ $product_order->shipping_address['email'] }}, Contact No : {{ $product_order->shipping_address['contact_no'] }}, Address : {{ $product_order->shipping_address['address'] }}</textarea>
                                            </div>
                                        </div>

                                    </fieldset>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <form action="{{ action('Admin\ProductOrderController@destroy',$product_order->id) }}" method="post" style="display: inline">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger btn-xs" name="remove_slider_group" type="submit" onclick="deleteSliderGroup()">Delete</button>
                                                </form>
                                                <a href="{{ action('Admin\ProductOrderController@delivered',$product_order->id) }}" class="btn sa-btn-primary btn-xs">Delivered</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end widget div -->
                        </div>
                        <!-- end widget -->

                    </div>
                    <!-- WIDGET ROW END -->

                </div>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>

    <div class="modal fade" id="confirm">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h2 class="modal-title">Delete order</h2>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <h4>Are you sure ?</h4>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-danger" id="delete">Yes</button>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function deleteSliderGroup() {
            $('#myModal').modal("show");
        }

        $('button[name="remove_slider_group"]').on('click', function(e) {
            var $form = $(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({
                backdrop: 'static',
                keyboard: false
            })
                .one('click', '#delete', function(e) {
                    $form.trigger('submit');
                });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    name : {
                        validators : {
                            notEmpty : {
                                message : 'Category name is required'
                            },
                        }
                    }
                }
            });

            // end profile form

        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
