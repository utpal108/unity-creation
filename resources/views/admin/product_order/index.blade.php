@extends('admin.layout.app')

@section('page_title','Admin | Product Orders')
@section('contents')
    <!-- BEGIN .sa-page-breadcrumb -->
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\ProductOrderController@index') }}">Product Orders</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa fa-table fa-fw "></i> Dashboard <span>> Product Orders</span></h1>
            </div>
        </div>
        <div>
            <div>
                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-12">
                            @include('flash::message')
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-0" data-widget-editbutton="false">
                                <header>
                                    <div class="widget-header">
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                        <h2>All product orders </h2>
                                    </div>
                                    <div class="widget-toolbar">
                                        <!-- add: non-hidden - to disable auto hide -->
                                    </div>
                                </header>
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body p-0">

                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                            <tr>
                                                <th data-hide="phone" class="text-center">ID</th>
                                                <th data-class="expand" class="text-center"> Order ID</th>
                                                <th data-class="expand" class="text-center"> Order By</th>
                                                <th data-class="expand" class="text-center"> Order Date</th>
                                                <th data-class="expand" class="text-center"> Total Price</th>
                                                <th data-class="expand" class="text-center"> Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($product_orders as $index=>$product_order)
                                                    <tr class="text-center">
                                                        <td>{{ $index+1 }}</td>
                                                        <td>{{ $product_order->order_id }}</td>
                                                        <td>{{ $product_order->ordered_by['name'] }}</td>
                                                        <td>{{ $product_order->created_at->format('Y-m-d') }}</td>
                                                        <td>{{ $product_order->payment['total_amount'] }}</td>
                                                        <td>
                                                            <a class="btn btn-success btn-xs" href="{{ action('Admin\ProductOrderController@show',$product_order->id) }}">Details</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->

                                    <!-- Bootstrap Modal -->
                                    <div class="modal fade" id="confirm">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">

                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                    <h2 class="modal-title">Delete slider group</h2>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>

                                                <!-- Modal body -->
                                                <div class="modal-body">
                                                    <h4>Are you sure ?</h4>
                                                </div>

                                                <!-- Modal footer -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                    <button type="submit" class="btn btn-danger" id="delete">Yes</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </article>
                        <!-- WIDGET END -->

                    </div>

                    <!-- end row -->

                    <!-- end row -->

                </section>
                <!-- end widget grid -->

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function deleteSliderGroup() {
            $('#myModal').modal("show");
        }

        $('button[name="remove_slider_group"]').on('click', function(e) {
            var $form = $(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({
                backdrop: 'static',
                keyboard: false
            })
                .one('click', '#delete', function(e) {
                    $form.trigger('submit');
                });
        });
    </script>
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
