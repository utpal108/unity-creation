@extends('admin.layout.app')

@section('page_title','Admin | Edit user')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\UserController@index') }}">Create user</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Edit user</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">
                @include('flash::message')
                <form id="sliderGroup" action="{{ action('Admin\UserController@update',$user->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Edit User </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>

                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Form Elements
                                            </legend>
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="name" value="{{ $user->name }}" placeholder="Full Name" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="Email Address" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Password (Optional)</label>
                                                <input type="password" class="form-control" name="password" placeholder="XXXXXXXXXX"  />
                                            </div>
                                            <div class="form-group">
                                                <label>Phone No</label>
                                                <div class="row" style="margin: 0px 0.5px">
                                                    <input type="text" class="form-control col-md-6" name="country_code" value="{{ $user->country_code }}" placeholder="Country Code" required />
                                                    <input type="text" class="form-control col-md-6" name="phone_no" value="{{ $user->phone_no }}" placeholder="Phone No" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>User Type</label>
                                                <select class="form-control" name="user_type">
                                                    <option value="user" {{ ($user->user_type =='user') ?? 'selected' }}>User</option>
                                                    <option value="admin" {{ ($user->user_type =='admin') ?? 'selected' }}>Admin</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Profession</label>
                                                <input type="text" class="form-control" name="profession" value="{{ $user->profession }}" placeholder="Profession" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Address</label>
                                                <textarea rows="5" class="form-control" name="address" required >{{ $user->address }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Select Role</label>
                                                <select multiple="" style="width: 100%; display: none;" class="select2" tabindex="-1" name="roles[]">
                                                    @foreach($roles as $role)
                                                        <option value="{{ $role->name }}" @if($user->hasRole($role->name)){{ 'selected' }} @endif>{{ $role->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-3">
                            <!-- /well -->
                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-tags"></i> Profile Image(130 X 130):</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-body text-center">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                    <img src="@if(!is_null($user->profile_image)){{ '/storage/' .$user->profile_image }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="profile image">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" name="profile_image">
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /well -->
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            // For Multiple Select
            if($.fn.select2) {
                $("select.select2").each(function(){var e=$(this),t=e.attr("data-select-width")||"100%";e.select2({allowClear:!0,width:t}),e=null});
            }
            if ($.fn.select2) {
                $("select.select2").each(function () {
                    var e = $(this), t = e.attr("data-select-width") || "100%";
                    e.select2({allowClear: !0, width: t}), e = null
                });
            }

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    // title : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider title is required'
                    //         },
                    //     }
                    // },
                    // subtitle : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider subtitle is required'
                    //         }
                    //     }
                    // }
                }
            });

            // end profile form

            $('div.alert').delay(3000).fadeOut(350);
        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
