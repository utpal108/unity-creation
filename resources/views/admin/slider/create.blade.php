@extends('admin.layout.app')

@section('page_title','Admin | Create slider')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\SliderGroupController@create') }}">Create slider</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Create slider</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <form id="sliderGroup" action="{{ action('Admin\SliderController@store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Crete Slider </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>

                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Form Elements
                                            </legend>
                                            <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" class="form-control" name="title" />
                                            </div>
                                            <div class="form-group">
                                                <label>Subtitle</label>
                                                <input type="text" class="form-control" name="subtitle" />
                                            </div>
                                            <div class="form-group">
                                                <label>Details</label>
                                                <input type="text" class="form-control" name="details" />
                                            </div>
                                            <div class="form-group">
                                                <label>Product URL</label>
                                                <div class="row" style="margin: 0px 1px">
                                                    <input name="product_url[name]" class="form-control col-lg-6" placeholder="URL Name" type="text">
                                                    <input name="product_url[url]" class="form-control col-lg-6" placeholder="URL" type="url">
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-group">
                                                <label>Slider Group</label>
                                                <select name="slider_group_id" class="form-control">
                                                    @foreach($slider_groups as $slider_group)
                                                        <option value="{{ $slider_group->id }}">{{ $slider_group->group_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Create
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-3">
                            <!-- /well -->
                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-tags"></i> Slider Image(1920X800):</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-body text-center">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                    <img src="http://placehold.it/200x200" width="100%" alt="header image">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" name="slider_image" required>
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /well -->
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    // title : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider title is required'
                    //         },
                    //     }
                    // },
                    // subtitle : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider subtitle is required'
                    //         }
                    //     }
                    // }
                }
            });

            // end profile form

        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
