
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('cart-component', require('./components/CartComponent.vue').default);
Vue.component('checkout-component', require('./components/CheckoutComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#main-page',
    data:{
        products:[],
        productVariationId:0,
        items:[],
        discount:0,
        total_items:0,
        cart_loading:true,
        coupon_code:true,
        coupon_loading:false,
        coupon_response:'',
    },
    methods:{
        getCartData(){
            var self=this;
            axios.get('/get_cart_data')
                .then(function (response) {
                    self.discount=response.data.discount_amount;
                    self.items=response.data.product_charts;
                    self.total_items=response.data.total_items;
                    self.cart_loading=false;
                });
        },

        updateCart(){
            axios.post('/update_cart_data', {
                cart_data: JSON.stringify(this.items),
            })
            .then(function (response) {

            })
        },
        addCoupon(coupon_code){
            this.coupon_loading=true;
            var self=this;
            axios.post('/add-coupon', {
                coupon_code: coupon_code,
            })
            .then(function (response) {
                self.coupon_response=response.data;
                self.coupon_code=false;
                self.coupon_loading=false;
            })
        },
        addToCart(product_id){
            var size = $("[name='size']:checked").val();
            var quantity = $("[name='quantity']").val();
            var self=this;
            axios.post('/add-new-cart', {
                product_id: product_id,
                variation_id: productVariationId,
                size: size,
                quantity: quantity,
            })
            .then(function (response) {
                $('#cart_product_name').html(response.data.name);
                $('#cart_product_image').attr('src',response.data.image);
                $('#card_added').modal('show');
                self.total_items++;
                self.getCartData();
            })
            .catch(function (error) {
                $('#warning-message').html(error.response.data);
                $('#warning-modal').modal('show');
            });
        }
    },
    computed:{
        subtotal_price(){
            var subtotal_price=0;
            this.items.forEach(function (item) {
                var product_price=parseFloat(item.quantity) * parseFloat(item.unit_price);
                subtotal_price+=product_price;
            });
            return subtotal_price;
        },
        total_price(){
            var subtotal_price=0;
            this.items.forEach(function (item) {
                var product_price=parseFloat(item.quantity) * parseFloat(item.unit_price);
                subtotal_price+=product_price;
            });
            var total_price=subtotal_price-this.discount;
            if (total_price<0){
                total_price=0;
            }
            return total_price;
        },
    },
    mounted(){
        this.getCartData();
    }
});
