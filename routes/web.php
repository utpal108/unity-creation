<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@show');

// For User Authentication
Route::get('/login', 'UserController@login');
Route::get('/register', 'UserController@user_register')->name('login');
Route::get('/logout', 'UserController@logout');
Route::post('/user-authenticate', 'UserController@user_authenticate');
Route::get('/user-logout', 'UserController@user_logout');
Route::post('/authenticate', 'UserController@authenticate');

// For User Confirmation Mail Verification
Route::get('/resend-email','Auth\VerificationController@resend')->name('verification.resend');
Route::get('/verify-email','Auth\VerificationController@show')->name('verification.notice');
Route::get('/verify-email/{id}','Auth\VerificationController@verify')->name('verification.verify');

Route::post('/subscribe', 'SubscriberController@store');
Route::get('/search-products/{search_item?}', 'ProductSearchController@index');
Route::get('/products/{search_item?}', 'ProductController@index');
Route::get('/category-wise-products/{category}', 'ProductController@categoryWiseProducts');
Route::get('/subcategory-wise-products/{category}', 'ProductController@subcategoryWiseProducts');
Route::get('/product-details/{id}', 'ProductController@product_details')->name('products.show');
Route::get('/view-chart', 'ProductCartController@index');
Route::get('/product-checkout', 'ProductCartController@product_checkout');
Route::post('/order-product', 'ProductCartController@order_product')->middleware('verified');
Route::post('/add-new-cart', 'ProductCartController@store');
Route::get('/remove-cart/{id}', 'ProductCartController@destroy');
Route::get('/get_cart_data', 'ProductCartController@get_cart_data');
Route::post('/update_cart_data', 'ProductCartController@update_cart_data');

Route::post('/user-registration', 'UserController@store');
Route::get('/news-details/{id}', 'BlogPostController@show')->name('blog.show');
Route::post('/create-blog-comment', 'BlogCommentController@store')->middleware('verified');
Route::post('/create-comment-replay', 'BlogCommentController@replay')->middleware('verified');
Route::get('/profile', 'UserController@profile')->middleware('verified');
Route::get('/edit-profile', 'UserController@edit_profile')->middleware('verified');
Route::post('/update-profile', 'UserController@update_profile')->middleware('verified');
Route::get('/provide-review/{order_id}', 'ProductReviewController@create')->middleware('verified');
Route::post('/provide-review', 'ProductReviewController@store')->middleware('verified');
Route::post('/send-contact-message', 'Admin\ContactMessageController@store');
Route::post('/usage-coupon-code', 'CouponController@usage');

//    For Ajax Call
Route::get('/get-subcategories/{id}','ProductController@getSubcategories')->middleware('isAdmin');
Route::get('/get-product-variations','ProductController@getProductVariations');
Route::post('/add-coupon','CouponController@addUserCoupon');


// SSLCOMMERZ Start
Route::get('/pay', 'PublicSslCommerzPaymentController@index')->middleware('verified');
Route::POST('/success', 'PublicSslCommerzPaymentController@success')->middleware('verified');
Route::POST('/fail', 'PublicSslCommerzPaymentController@fail')->middleware('verified');
Route::POST('/cancel', 'PublicSslCommerzPaymentController@cancel')->middleware('verified');
Route::POST('/ipn', 'PublicSslCommerzPaymentController@ipn')->middleware('verified');

//SSLCOMMERZ END

// For Admin Panel
// ===============================

Route::group(['prefix'=>'/admin', 'middleware'=>'isAdmin', 'namespace'=>'Admin'],function (){

    Route::get('/', 'DashboardController@index');

//    For Dynamic Menu
//    ==================================

    Route::get('/menu', 'MenuController@index');
    Route::post('/menu/create', 'MenuItemController@store');
    Route::get('/menu/delete/{id}', 'MenuItemController@destroy');
    Route::get('/menu/load_data', 'MenuController@loadItems');
    Route::post('/menu/update', 'MenuItemController@update');
    Route::post('/menu/order', 'MenuController@order_item')->name('menus.order');

//    For Dynamic Slider
//    =====================================
    Route::resource('/slider-groups', 'SliderGroupController');
    Route::resource('/sliders', 'SliderController');

//    For Dynamic Page
    Route::post('/load_template','PageController@load_template');
    Route::resource('/pages', 'PageController');

//    For Products
    Route::resource('/product-categories','ProductCategoryController');
    Route::resource('/product-subcategories','ProductSubcategoryController');
    Route::resource('/products','ProductController');

//    For Blog
    Route::resource('/blog-posts','BlogController');

//    For Site Settings
//    =====================================
    Route::get('/site-settings', 'SiteSettingController@index');
    Route::post('/site-settings', 'SiteSettingController@update');

//    For Widget
//    =====================================
    Route::resource('/widgets', 'WidgetController');

//    For Subscribers
//    =====================================
    Route::resource('/subscribers', 'SubscriberController');

//    For Product Orders
//    =====================================
    Route::get('/deliver-product/{id}', 'ProductOrderController@delivered');
    Route::get('/product-delivered', 'ProductOrderController@delivered_products');
    Route::resource('/product-orders', 'ProductOrderController');

//    For Role & Permissions
    Route::resource('/modules','ModuleController');
    Route::resource('/user-permissions','UserPermissionController');
    Route::resource('/user-roles','UserRoleController');

//    For Users
    Route::get('/users-pending','UserController@pending_users');
    Route::get('/approve-user/{id}','UserController@approve_user');
    Route::get('/wholesalers','UserController@wholesalers');
    Route::resource('/users','UserController');

//    For Coupons
    Route::resource('/coupons','CouponController');


});

// For Viw Pages
// ===========================

Route::get('/{slug}','PageController@show')->name('page.show');
